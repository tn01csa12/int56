#include <stdio.h>

#include "vector2D.h"

#define R 640
#define C 640


void gaussianFilter();

typedef struct
{
	 unsigned char	color[R][C*3];
}table;


struct XYCOordinates { 
    int x, y; 
}; 

typedef struct XYCOordinates center; 

unsigned char input[R][C*3];
table data2;

center blob_detection(char filename[])
{
	int i,j,linkListLength,size,Xc,Yc;
    FILE *image;
	center c;
	
	
	unsigned char b;//only for compying a image

		 
        image = fopen(filename, "rb" );
		
		
		

        if(image == NULL)
        {
            printf("The file cannot be opened.\n");
        }
		
        else
        {
			
			fseek(image, 54, SEEK_SET); //move the pointer by 54 bytes to skip the Header and the Infoheader
			
			fread(&data2, sizeof(unsigned char), sizeof(data2), image);
        }
		
		
		//copy the array 
		for(i=0;i<R;i++)
			{
				for(j=0;j<C*3;j++)
				{
					input[i][j] = data2.color[i][j];
				}
			}
			
			gaussianFilter();
			
			
		// apply sequential search algorithm
		 for(i=0;i<R;i++)
			{
				for(j=0;j<(C*3);j=j+3)
				{					 
					addToList(input[i][j],input[i][j+1],input[i][j+2],i,j);
				}					 
			} 
	 
		 centerCalculator();
		 
		 blob_Selecter(0,0,255);
		 
		 
		 c.y=blobMatched->Xcenter;
		 c.x=blobMatched->Ycenter;
		
		fclose(image); 
		return c;

}


void gaussianFilter()
{
	int i,j,k,l;
	for(i=1;i<(R-1);i++)
			{
				for(j=3;j<((C*3)-1);j=j+3)
				{					 
					input[i][j] = ((1*data2.color[i-1][j-3]) + (2*data2.color[i-1][j]) +(1*data2.color[i-1][j+3]) +(2*data2.color[i][j-3]) +(4*data2.color[i][j]) +(2*data2.color[i][j+3]) +(1*data2.color[i+1][j-3]) +(2*data2.color[i+1][j]) +(1*data2.color[i+1][j+3]))/16;
					input[i][j+1] =  ((1*data2.color[i-1][j-2]) + (2*data2.color[i-1][j+1]) +(1*data2.color[i-1][j+4]) +(2*data2.color[i][j-2]) +(4*data2.color[i][j+1]) +(2*data2.color[i][j+4]) +(1*data2.color[i+1][j-2]) +(2*data2.color[i+1][j+1]) +(1*data2.color[i+1][j+4]))/16;
					input[i][j+2] =  ((1*data2.color[i-1][j-1]) + (2*data2.color[i-1][j+2]) +(1*data2.color[i-1][j+5]) +(2*data2.color[i][j-1]) +(4*data2.color[i][j+2]) +(2*data2.color[i][j+5]) +(1*data2.color[i+1][j-1]) +(2*data2.color[i+1][j+2]) +(1*data2.color[i+1][j+5]))/16;	
				}
			} 
}
