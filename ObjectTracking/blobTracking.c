#include <stdio.h>
#include<math.h>

#include "blob_detection.h"

#define fovx 60
#define fovy 60
#define width 640
#define height 640

double pan,tilt;

// fovx is field of view  in horizontal plane
// fovy is field of view in verticle plane

void AngleCalculater(int ,int );
int shiftOfOrigin(int ,int);



int main(void)
{
	
	center c1,c2,c3,c4,c5,c6,c7;
	c1=blob_detection("atop.bmp");
	//c2=blob_detection("aright.bmp");
	//c3=blob_detection("aBottom.bmp");
	//c4=blob_detection("aleft.bmp");
	//c5=blob_detection("acent.bmp");
	//c6=blob_detection("bottomLeft.bmp");
	//c7=blob_detection("bottomLeft.bmp");
	
	AngleCalculater(((c1.x+1)/3),c1.y);
	//AngleCalculater(((c2.x+1)/3),c2.y);
	//AngleCalculater(((c3.x+1)/3),c3.y);
	//AngleCalculater(((c4.x+1)/3),c4.y);
	//AngleCalculater(((c5.x+1)/3),c5.y);
	//AngleCalculater(((c6.x+1)/3),c6.y);
	//AngleCalculater(((c7.x+1)/3),c7.y);
	
}	

void AngleCalculater(int x,int y)
{
	printf("x->%d\ty->%d\n",x,y);
	x=shiftOfOrigin(x,width/2);
	y=shiftOfOrigin(y,height/2);
	printf("x->%d\ty->%d\n",x,y);

	pan = ((x*fovx)/(width));
	tilt = ((y*fovy)/(height));
	printf("pan->%lf\ttilt->%lf\n\n",pan,tilt);
}

//function returns the shifted coordinate value
//takes input of coordinate value and the max size of that parameter eg width/height
int shiftOfOrigin(int coord,int max)
{
	int newCoord;
	newCoord = coord-max;
	return newCoord;
}	
	

	
	