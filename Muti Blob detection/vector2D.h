#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int newColorChecker(int,int,int);
void addToList(int, int, int,int, int);	
int return_blue_pointer_location(int);

struct colorList {
	
  int color[3];
  int length;
  int sum_of_coordinates_of_Blob_Xcord;
  int sum_of_coordinates_of_Blob_Ycord;
  int Xcenter;
  int Ycenter;
  struct coordinates *coordinatesHead;
  struct colorList *next;
};


struct coordinates {
	
	int y;	
	int x;
	struct coordinates *next;
};

struct colorList *colorListHead = NULL;

struct colorList *colorMatched; //used for storing the coordinates if the color are found in the list


void addToList(int colorB,int colorG,int colorR, int x, int y) {	
	 
	if(newColorChecker(colorB,colorG,colorR))
	{
		// if no color present make a new link of colorLink type
		struct colorList *colorListLink = (struct colorList*) malloc(sizeof(struct colorList));
		colorListLink->color[0] =  colorB;
		colorListLink->color[1] =  colorG;
		colorListLink->color[2] =  colorR;
		colorListLink->next = colorListHead;
		colorListHead = colorListLink;
		
		// allocate memory for link of coordinates type 
		struct coordinates *coordinatesListHead = NULL; 
		struct coordinates *coordinatesLink = (struct coordinates*) malloc(sizeof(struct coordinates));
		coordinatesLink->x = x;
		coordinatesLink->y = y;
		coordinatesLink->next = coordinatesListHead;
		colorListLink->coordinatesHead = coordinatesLink;
		colorListLink->length = 1;
		colorListLink->sum_of_coordinates_of_Blob_Xcord=x;
		colorListLink->sum_of_coordinates_of_Blob_Ycord=y;
		colorListLink->Xcenter=0;
		colorListLink->Ycenter=0;
		
	}
	
	else{
		//if color is present 
		struct coordinates *coordinatesLink = (struct coordinates*) malloc(sizeof(struct coordinates));	
		coordinatesLink->x = x;
		coordinatesLink->y = y;
		coordinatesLink->next = colorMatched->coordinatesHead;
		colorMatched->coordinatesHead = coordinatesLink;	
		colorMatched->length = (colorMatched->length)+1;
		colorMatched->sum_of_coordinates_of_Blob_Xcord=(colorMatched->sum_of_coordinates_of_Blob_Xcord)+ x;
		colorMatched->sum_of_coordinates_of_Blob_Ycord=(colorMatched->sum_of_coordinates_of_Blob_Ycord)+y;
	}
}

//prints the list with RGB values and the coordinates
void printList() 
{
	struct colorList *ptr = colorListHead;
	// if the list has no vlaue then this prints
	if(ptr==NULL)
	{
		printf("\nList Empty\n");
		
		return;
	}
	while(ptr!=NULL)
	{
		printf("\ncolor\tR=%d,\tG=%d,\tB=%d\n",ptr->color[2],ptr->color[1],ptr->color[0]);
		struct coordinates *currentCoordinate = ptr->coordinatesHead; 
		while(currentCoordinate!=NULL)
		{
			printf("(%d,%d) ",currentCoordinate->x,currentCoordinate->y);
			currentCoordinate = currentCoordinate->next;
		}
		ptr = ptr->next;
	}
}



//function to check if Color List is already present in the list or not
//returns -1 if list is empty or list has no matching color
//returns -0 if color is presnt and also sets the "colorMatched*" to the colorList link which has the color present 
int newColorChecker(int colorB,int colorG,int colorR)
{
	struct colorList *ptr = colorListHead;
	
	if(ptr==NULL)
	{
		return 1;
	}
	
	else{
		
	while(ptr != NULL)
	{
		if(ptr->color[0]==colorB &&  ptr->color[1]==colorG && ptr->color[2]==colorR) 
		{
			colorMatched = ptr;//if color is found store the ptr in the variable for adding coordinates
			return 0;
		}
		ptr=ptr->next;
	}
	return 1;
	}	
}

void centerCalculator()
{
	struct colorList *ptr = colorListHead;

	
	while(ptr!=NULL)
	{
		(ptr->Xcenter) = (ptr->sum_of_coordinates_of_Blob_Xcord)/(ptr->length);
		(ptr->Xcenter) = return_blue_pointer_location((ptr->Xcenter));
		(ptr->Ycenter) = (ptr->sum_of_coordinates_of_Blob_Ycord)/(ptr->length);
		 ptr=ptr->next;
	}
	
	ptr = colorListHead;	
	while(ptr!=NULL)
	{

		 printf("Xcenter= %d\t",(ptr->Xcenter));
		 printf("Ycenter= %d\t",(ptr->Ycenter));
		 printf("Bcolor= %d\t",(ptr->color[0]));
		 printf("Gcolor= %d\t",(ptr->color[1]));
		 printf("Rcolor= %d\t",(ptr->color[2]));
		 
		 printf("\n");
 		ptr=ptr->next;

	}
	return;
}


int return_blue_pointer_location(int x)
{
	
	if(x%3==0){
		return x;
	}
	else if(x%3==1)
	{
		return (x-1);
	}
	else if(x%3==2)
	{
		return (x-2);
	}
}
