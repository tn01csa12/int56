#include <stdio.h>

#include "vector2D.h"

#define R 320
#define C 320

void gaussianFilter();


typedef struct
{
	 unsigned char	color[R][C*3];
}table;




//returns blue color index location in matrix as the return value can be
//at any location
int return_blue_pointer_location(int);

//print plus at the center of blob
void print_plus_center_blob(int ,int );

unsigned char input[R][C*3];

table data2;

int main ( int argc, char *argv[] )
{
	int i,j,linkListLength,c,size,Xc,Yc;
    char filename[256];
    FILE *image;
	FILE *outImage;
	
	
	unsigned char b;//only for compying a image

		printf("Enter File Name: \n");        
		scanf("%s", &filename);  
        image = fopen(filename, "rb" );
		
		
		

        if(image == NULL)
        {
            printf("The file cannot be opened.\n");
        }
		
        else
        {
			
			fseek(image, 54, SEEK_SET); //move the pointer by 54 bytes to skip the Header and the Infoheader
			
			fread(&data2, sizeof(unsigned char), sizeof(data2), image);
        }
		
		
		//copy the array 
		for(i=0;i<R;i++)
			{
				for(j=0;j<C*3;j++)
				{
					input[i][j] = data2.color[i][j];
				}
			}
			gaussianFilter();
			
		// apply sequential search algorithm
		 for(i=0;i<R;i++)
			{
				for(j=0;j<(C*3);j=j+3)
				{					 
					addToList(input[i][j],input[i][j+1],input[i][j+2],i,j);
				}					 
			} 
			 
		 centerCalculator();
			
			
		
		//prints plus at center of blob
		struct colorList *ptr = colorListHead;
				
				
				
		while(ptr!=NULL)
		{
			/*the if condition checks for blobsize larger than 1000 and colors are less then the threshold
			# to eliminate the background white color from being considered as a blob
			*/
			if(((ptr->length)>4000) && ((ptr->color[0]<200) || (ptr->color[1]<200) || (ptr->color[2]<200)) )
			{
		Xc=ptr->Xcenter;
		Yc=ptr->Ycenter;
		print_plus_center_blob(Xc,Yc);
		printf("\nXc->%d\tYc->%d",Xc,Yc);
		ptr=ptr->next;

			}
			else{
				ptr=ptr->next;
			}
			
		}
		
		
		
	
					//to copy the image  and make new image

			 outImage = fopen("Output.bmp", "wb" );
			fseek(image, 0, SEEK_SET);
			while ((c = fgetc(image)) != EOF)
			{
				fputc(c, outImage);
			} 
			
			
			
			// update the array in image to select the blob
			 fseek(outImage, 54, SEEK_SET);	  
			fwrite(input,1,(R*C*3), outImage);
			
			//printf("\nsize of array is:%d",size);
				 
		
		fclose(outImage); 
		fclose(image); 
return 0;

}





// i-row,j-column

//print plus at the center of blob
void print_plus_center_blob(int Y,int X)
{
		
		input[Y+1][X] = 255;
		input[Y+1][X+1] = 255;
		input[Y+1][X+2] = 255;

		input[Y+1][X+3] = 255;
		input[Y+1][X+3+1] = 255;
		input[Y+1][X+3+2] = 255;


		input[Y][X+3] = 255;
		input[Y][X+3+1] = 255;
		input[Y][X+3+2] = 255;

		input[Y-1][X+3] = 255;
		input[Y-1][X+3+1] = 255;
		input[Y-1][X+3+2] = 255;
		
		
		input[Y-1][X] = 255;
		input[Y-1][X+1] = 255;
		input[Y-1][X+2] = 255;


		input[Y-1][X-3] = 255; 
		input[Y-1][(X-3)+1] = 255; 
		input[Y-1][(X-3)+2] = 255; 

				
		
		input[Y][(X-3)] = 255;
		input[Y][(X-3)+1] =255;
		input[Y][(X-3)+2] = 255;

				
		
		input[Y+1][X-1] = 255;
		input[Y+1][(X-1)+1] = 255;
		input[Y+1][(X-1)+2] = 255;

		input[Y+1][(X-3)] = 255;
		input[Y+1][(X-3)+1] = 255;
		input[Y+1][(X-3)+2] = 255;
		
		input[Y][(X)] = 255;
		input[Y][(X+1)] = 255;
		input[Y][(X+2)] = 255;

		
}



void gaussianFilter()
{
	int i,j,k,l;
	for(i=1;i<(R-1);i++)
			{
				for(j=3;j<((C*3)-1);j=j+3)
				{					 
					input[i][j] = ((1*data2.color[i-1][j-3]) + (2*data2.color[i-1][j]) +(1*data2.color[i-1][j+3]) +(2*data2.color[i][j-3]) +(4*data2.color[i][j]) +(2*data2.color[i][j+3]) +(1*data2.color[i+1][j-3]) +(2*data2.color[i+1][j]) +(1*data2.color[i+1][j+3]))/16;
					input[i][j+1] =  ((1*data2.color[i-1][j-2]) + (2*data2.color[i-1][j+1]) +(1*data2.color[i-1][j+4]) +(2*data2.color[i][j-2]) +(4*data2.color[i][j+1]) +(2*data2.color[i][j+4]) +(1*data2.color[i+1][j-2]) +(2*data2.color[i+1][j+1]) +(1*data2.color[i+1][j+4]))/16;
					input[i][j+2] =  ((1*data2.color[i-1][j-1]) + (2*data2.color[i-1][j+2]) +(1*data2.color[i-1][j+5]) +(2*data2.color[i][j-1]) +(4*data2.color[i][j+2]) +(2*data2.color[i][j+5]) +(1*data2.color[i+1][j-1]) +(2*data2.color[i+1][j+2]) +(1*data2.color[i+1][j+5]))/16;	
				}
			} 
}