#include <stdio.h>
#include<string.h>
#include "vector.h"

#define R 640
#define C 640
#define blobColor 255
#define setColor 254


typedef struct
{
	 unsigned char	color[R][C*3];
}table;


int grass_fire(int,int);

//returns blue color index location in matrix as the return value can be
//at any location
int return_blue_pointer_location(int);

//print plus at the center of blob
void print_plus_center_blob(int ,int );

unsigned char input[R][C*3];



int main ( int argc, char *argv[] )
{
	int i,j,linkListLength,Xcenter,Ycenter,c,size;
    char filename[256];
	char folder_path[] = "./images/";
    FILE *image;
	FILE *outImage;
	
	table data2;
	unsigned char b;//only for compying a image

		printf("Enter File Name:(Enter 640.bmp) \n");        
		scanf("%s", &filename);
		strcat(folder_path,filename);		
        image = fopen(folder_path, "rb" );
		
		
		

        if(image == NULL)
        {
            printf("The file cannot be opened.\n");
        }
		
        else
        {
			
			fseek(image, 54, SEEK_SET); //move the pointer by 54 bytes to skip the Header and the Infoheader
			
			fread(&data2, sizeof(unsigned char), sizeof(data2), image);
			
			
			
			//print the original image array
			 /*  for(i=0;i<R;i++)
			{
				
				//0-B, 1-G, 2-R
				for(j=2;j<C*3;j=j+3)
				{
					printf("%d\t",data2.color[i][j]);
				}
				printf("\n%d##",i);
			}
			  */
        }
		
		//copy the array 
		for(i=0;i<R;i++)
			{
				for(j=0;j<C*3;j++)
				{
					input[i][j] = data2.color[i][j];
				}
			}
			
			//print copy of array
		 /* for(i=0;i<R;i++)
			{
				
				//0-B, 1-G, 2-R
				for(j=2;j<C*3;j=j+3)
				{
					printf("%d\t",input[i][j]);
				}
				printf("\n%d##",i); 
			} */

			
		// apply grassfire algorithm
		 for(i=1;i<(R-1);i++)
			{
				for(j=5;j<((C*3)-3);j=j+3)
				{
					grass_fire(i,j);
					
				}
			} 
			
	
		 
		//print the list
		 //printList();
		
		//total length of the list of pixels
		linkListLength = length();
		
		Xcenter=sum_of_coordinates_of_Blob_width()/linkListLength;
		
		// length is 3 times the value
		Ycenter=sum_of_coordinates_of_Blob_height()/linkListLength;
		
	
		Xcenter = return_blue_pointer_location(Xcenter);
		
		//prints plus at center of blob
		
		print_plus_center_blob(Xcenter,Ycenter);
		print_plus_center_blob(Xcenter+9,Ycenter);
		print_plus_center_blob(Xcenter,Ycenter+3);
		print_plus_center_blob(Xcenter-9,Ycenter);
		print_plus_center_blob(Xcenter,Ycenter-3);
		
		
		
		//print the coordinate of the center of blob 
		printf("\nX:%d,Y:%d",Xcenter,Ycenter);
		printf("\nOutput saved in images folder");
		
		//print array after updating the pixel values and finding the center
		 /* for(i=0;i<R;i++)
			{
				
				//0-B, 1-G, 2-R
				for(j=2;j<C*3;j=j+3)
				{
					printf("%d\t",input[i][j]);
				}
				printf("\n%d##",i);
			} 
			
	   */
					//to copy the image  and make new image

			 outImage = fopen("./images/Output.bmp", "wb" );
			fseek(image, 0, SEEK_SET);
			while ((c = fgetc(image)) != EOF)
			{
				fputc(c, outImage);
			} 
			
			
			
			// update the array in image to select the blob
			 fseek(outImage, 54, SEEK_SET);	  
			fwrite(input,1,(R*C*3), outImage);
			
			//printf("\nsize of array is:%d",size);
				 
		
		fclose(outImage); 
		fclose(image);
		
return 0;


}


int return_blue_pointer_location(int x)
{
	
	if(x%3==0){
		return x;
	}
	else if(x%3==1)
	{
		return (x-1);
	}
	else if(x%3==2)
	{
		return (x-2);
	}
}



// i-row,j-column
//The grass_fire algorithm


int grass_fire(int i,int j)
{
	
	//printf("%d,%d\n",i,j);
 if(input[i][j+3]==blobColor)
 {
	input[i][j+3]=setColor;
	input[i][(j+3)-2]=setColor;
	input[i][(j+3)-1]=setColor;

	addToList(i,j+3);
	grass_fire(i,j+3);
 } 
 else if(input[i+1][j]==blobColor)
 {
	 input[i+1][j] = setColor;
	 input[i+1][j-2] = setColor;
	input[i+1][j-1] = setColor;

	 addToList(i+1,j);
	 grass_fire(i+1,j);
 }
 else if(input[i][j-3]==blobColor)
 {
	 input[i][j-3] = setColor;
	 input[i][((j-3)-2)] = setColor;
	 input[i][((j-3)-1)] = setColor;


	 addToList(i,j-3);
	 grass_fire(i,j-3);
 }
 else if(input[i-1][j]==blobColor)
 {
	 input[i-1][j] = setColor;
	input[i-1][j-2] = setColor;
	input[i-1][j-1] = setColor;

	 addToList(i-1,j);
	 grass_fire(i-1,j);
 } 
 return -1;
}



//print plus at the center of blob
void print_plus_center_blob(int X,int Y)
{
		
		input[Y+1][X] = 0;
		input[Y+1][X+1] = 0;
		input[Y+1][X+2] = 0;

		input[Y+1][X+3] = 0;
		input[Y+1][X+3+1] = 0;
		input[Y+1][X+3+2] = 0;


		input[Y][X+3] = 0;
		input[Y][X+3+1] = 0;
		input[Y][X+3+2] = 0;

		input[Y-1][X+3] = 0;
		input[Y-1][X+3+1] = 0;
		input[Y-1][X+3+2] = 0;
		
		
		input[Y-1][X] = 0;
		input[Y-1][X+1] = 0;
		input[Y-1][X+2] = 0;


		input[Y-1][X-3] = 0; 
		input[Y-1][(X-3)+1] = 0; 
		input[Y-1][(X-3)+2] = 0; 

				
		
		input[Y][(X-3)] = 0;
		input[Y][(X-3)+1] = 0;
		input[Y][(X-3)+2] = 0;

				
		
		input[Y+1][X-1] = 0;
		input[Y+1][(X-1)+1] = 0;
		input[Y+1][(X-1)+2] = 0;

		input[Y+1][(X-3)] = 0;
		input[Y+1][(X-3)+1] = 0;
		input[Y+1][(X-3)+2] = 0;
		
		input[Y][(X)] = 0;
		input[Y][(X+1)] = 0;
		input[Y][(X+2)] = 0;

		
}
