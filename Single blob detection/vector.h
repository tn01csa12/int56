#include <stdio.h>
#include <stdlib.h>
#include <math.h>
struct node {
   int y;	
   int x;
   struct node *next;
};


struct node *head = NULL;
struct node *current = NULL;


void addToList(int x, int y) {

   struct node *link = (struct node*) malloc(sizeof(struct node));
	
   link->x = x;
   link->y = y;
   link->next = head;
   head = link;
}

void printList() {
   struct node *ptr = head;
   printf("\n[ ");
	
      while(ptr != NULL) {
      printf("(%d,%d) ",ptr->x,ptr->y);
      ptr = ptr->next;
   }
	
   printf(" ]");
}




int length() {
   int length = 0;
   struct node *current;
	
   for(current = head; current != NULL; current = current->next) {
      length++;
   }
	
   return length;
}

int sum_of_coordinates_of_Blob_height()
{
	int XCoord=0;

   struct node *ptr = head;

	while(ptr != NULL) {
	XCoord = XCoord + ptr->x;
    ptr = ptr->next;
   }
   
	return round(XCoord);
	
}
int sum_of_coordinates_of_Blob_width()
{
	int YCoord=0;

   struct node *ptr = head;

	while(ptr != NULL) {
	YCoord = YCoord + ptr->y;
    ptr = ptr->next;
   }
  
	return round(YCoord);
}