#include <stdio.h>
#include<string.h>
#define R 24
#define C 24
#pragma pack(push,1)

typedef struct
{
unsigned char FM1; /* 'B' */
unsigned char FM2; /* 'M' */
unsigned int Filesize;
unsigned short unused1;
unsigned short unused2;
unsigned int Offset; /* Offset to the start of image data */
}header;
 
typedef struct
{
	unsigned int size;
	unsigned int width;
	unsigned int height;
	unsigned short int  planes;
	unsigned short int  bitcount;
	unsigned int compression;
	unsigned int ImageSize;
	unsigned int Xpixel;
	unsigned int Ypixel;
	unsigned int colorUsed;
	unsigned int colorImportaht;	
}infoHeader;

typedef struct
{
	 unsigned char	  color[R][C*3];
	 

}table;

 
 
#pragma pop

int main ( int argc, char *argv[] )
{
	int i,j;
    char filename[256];
	char folder_path[] = "./images/";
    FILE *image;
    header data;
	infoHeader data1;
	table data2;
 
                printf("Enter File Name:(Put 24x24.bmp ) \n");        
				scanf("%s", &filename);
  
	    strcat(folder_path,filename);
		           		printf("%s",folder_path);

        image = fopen(folder_path, "rb" );
 
 
        fread(&data, sizeof(unsigned char), sizeof(data), image);
		fread(&data1, sizeof(unsigned char), sizeof(data1),image);
        fread(&data2, sizeof(short int), sizeof(data2), image);
         
        if(image == NULL)
        {
            printf("The file cannot be opened.\n");
 
 
        }
 
 
        else
        {
			printf("\nHeader\n________________\n");

						
            printf("fileMarker1 = %c\n", data.FM1);
 
 
            printf("fileMarker2 = %c\n", data.FM2);
 
 
            printf("bfSize = %d\n", data.Filesize);
 
 
            printf("unused 1 = %d\n", data.unused1);
 
 
            printf("unused 2 = %d\n", data.unused2); 
 
 
            printf("imagedataoffset = %d\n",data.Offset);
			
			printf("\nInfoHeader\n______________________\n");
			
			printf("Size = %d\n", data1.size);
			
			printf("Width = %d\n", data1.width);

			printf("Height = %d\n", data1.height);
			
			printf("Planes = %hu\n", data1.planes);
			
			printf("BitCount = %hu\n", data1.bitcount);
			
			printf("Compression = %d\n", data1.compression);
			
			printf("ImageSize = %d\n", data1.ImageSize);
			
			printf("Xpixel = %d\n", data1.Xpixel);
			
			printf("Ypixel = %d\n", data1.Ypixel);
			
			printf("Color used = %d\n", data1.colorUsed);
 
			printf("Color Import = %d\n", data1.colorImportaht);
			
			printf("______________________________________\n");
			
			
			printf("\nThe order of color is BGR\n");

			for(i=0;i<R;i++)
			{
				printf("\n%d->",i);
				for(j=0;j<C*3;j=j+3)
				{
					printf("%d,%d,%d\t",data2.color[i][j],data2.color[i][j+1],data2.color[i][j+2]);
				}
				printf("\n%d",i);
			}
			
        }
return 0;
}